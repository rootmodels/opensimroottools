# OpenSimRoot Tools - Copyright (C) - 2019 - Ernst Schäfer
# All rights reserved.
#
# This file is part of OpenSimRoot Tools.
#
# OpenSimRoot Tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# OpenSimRoot Tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This file contains a collection of scripts to manipulate OpenSimRoot XML input files.

from GeneralFunctions import *
import tempfile
from xml.etree import ElementTree as ET

# This function makes an XML file more readable by making sure the indentation is correct and every tag is written on one line where possible.
def CleanFileKeepComments(inputString):
	lines = inputString.splitlines()
	cleanString = ""
	comment = False
	indentNumber = 0
	previousIndentNumber = 0
	for line in lines:
		if line[0:1] == "$":
			cleanString += '\n' + line.rstrip()
			continue
		lineCopy = line.strip().rstrip().replace("'", '"')
		countCopy = lineCopy
		if comment is True:
			if line.partition("-->")[1] != "":
				comment = False
			countCopy = line.partition("-->")[2].rstrip()
		templist = countCopy.split("<!--")
		countCopy = templist[0]
		for i in range(1, len(templist)):
			if templist[i].rpartition("-->")[1] != "":
				countCopy += templist[i].rpartition("-->")[2].rstrip()
			comment = True
		if "-->" in templist[len(templist)-1]:
			comment = False
		if "<Simula" in lineCopy:
			for i in range(indentNumber):
				lineCopy = '\t' + lineCopy
			lineCopy = '\n' + lineCopy
		if "<!--" in lineCopy:
			for i in range(indentNumber):
				lineCopy = '\t' + lineCopy
			lineCopy = '\n' + lineCopy
		indentNumber -= countCopy.count("</Simula")
		indentNumber -= countCopy.count("/>")
		indentNumber += countCopy.count("<Simula")
		if len(lineCopy) > 0:
			if not lineCopy[-1] == '>':
				lineCopy += ' '
		if previousIndentNumber > indentNumber:
			for i in range(indentNumber):
				lineCopy = '\t' + lineCopy
			lineCopy = '\n' + lineCopy
			previousIndentNumber = indentNumber
		if "</Simula" in lineCopy:
			previousIndentNumber = indentNumber
		if "/>" in lineCopy:
			previousIndentNumber = indentNumber
		if "<?xml" in lineCopy:
			lineCopy = '\n' + lineCopy.strip().rstrip()
		cleanString += lineCopy
	for i in range(10):
		cleanString = cleanString.replace(' <', '<')
		cleanString = cleanString.replace('> ', '>')
		cleanString = cleanString.replace(' />', '/>')
		cleanString = cleanString.replace('"  ', '" ')
	return cleanString

# This function makes an XML file more readable by removing all comments and making sure the indentation is correct and every tag is written on one line where possible.
def CleanFile(inputString):
	comment = False
	noCommentString = ""
	i = 0
	while True:
		if inputString[int(i): int(i + 4)] == "<!--":
			comment = True
			i += 4
			continue
		if inputString[i: i + 3] == "-->":
			comment = False
			i += 3
			continue
		if i == len(inputString):
			break
		if not comment:
			noCommentString += inputString[i]
		i += 1
	lines = noCommentString.splitlines()
	cleanString = ""
	indentNumber = 0
	previousIndentNumber = 0
	for line in lines:
		lineCopy = line.strip().rstrip().replace("'", '"')
		if "<Simula" in lineCopy:
			for i in range(indentNumber):
				lineCopy = '\t' + lineCopy
			lineCopy = '\n' + lineCopy
		indentNumber -= lineCopy.count("</Simula")
		indentNumber -= lineCopy.count("/>")
		indentNumber += lineCopy.count("<Simula")
		if len(lineCopy) > 0:
			if not lineCopy[-1] == '>':
				lineCopy += ' '
		if previousIndentNumber > indentNumber:
			for i in range(indentNumber):
				lineCopy = '\t' + lineCopy
			lineCopy = '\n' + lineCopy
			previousIndentNumber = indentNumber
		if "</Simula" in lineCopy:
			previousIndentNumber = indentNumber
		if "/>" in lineCopy:
			previousIndentNumber = indentNumber
		if "<?xml" in lineCopy:
			lineCopy = '\n' + lineCopy.strip().rstrip()
		cleanString += lineCopy
	for i in range(10):
		cleanString = cleanString.replace(' <', '<')
		cleanString = cleanString.replace('> ', '>')
		cleanString = cleanString.replace(' />', '/>')
		cleanString = cleanString.replace('"  ', '" ')
	return cleanString	

# This function makes an XML file more readable by removing all comments and making sure the indentation is correct and every tag is written on one line where possible.
def CleanFileOld(inputString):		
	lines = inputString.splitlines()
	parsedString = ""
	cleanString = ""
	comment = False
	indentNumber = 0
	previousIndentNumber = 0
	for line in lines:
		lineCopy = line.strip().rstrip().replace("'", '"')
		if comment is True:
			if line.partition("-->")[1] != "":
				comment = False
			lineCopy = line.partition("-->")[2].rstrip()
		templist = lineCopy.split("<!--")
		lineCopy = templist[0]
		for i in range(1, len(templist)):
			if templist[i].rpartition("-->")[1] != "":
				lineCopy += templist[i].rpartition("-->")[2].rstrip() + ' '
			comment = True
		if "-->" in templist[len(templist)-1]:
			comment = False
		if "<Simula" in lineCopy:
			for i in range(indentNumber):
				lineCopy = '\t' + lineCopy
			lineCopy = '\n' + lineCopy
			tempList = lineCopy.partition("<Simula")
			if "<Simula" in tempList[2]:
				lineCopy = tempList[0] + tempList[1]
				tempList = tempList[2].partition("<Simula")
				lineCopy += tempList[0].rstrip() + '\n'
				for i in range(indentNumber + 1):
					lineCopy += '\t'
				lineCopy += tempList[1] + tempList[2]
#				lineCopy = tempList[0] + tempList[1]
#				tempCounter = 1
#				while "<Simula" in tempList[2]:
#					tempList = tempList[2].partition("<Simula")
#					lineCopy += tempList[0].rstrip()
#					lineCopy += '\n'
#					for i in range(indentNumber + tempCounter):
#						lineCopy += '\t'
#					tempCounter += 1
#					lineCopy += tempList[1]
#				lineCopy += tempList[2]
		indentNumber -= lineCopy.count("</Simula")
		indentNumber -= lineCopy.count("/>")
		indentNumber += lineCopy.count("<Simula")
		if previousIndentNumber > indentNumber:
			for i in range(indentNumber):
				lineCopy = '\t' + lineCopy
			lineCopy = '\n' + lineCopy
			previousIndentNumber = indentNumber
		if "</Simula" in lineCopy:
			tempList = lineCopy.partition("</Simula")
			if "<Simula" in tempList[2]:
				lineCopy = '\n' + tempList[0][2:] + tempList[1]
				tempList = tempList[2].partition("<Simula")
				lineCopy += tempList[0].rstrip() + '\n'
				for i in range(indentNumber -1):
					lineCopy += '\t'
				lineCopy += tempList[1] + tempList[2]
			if "</Simula" in tempList[2]:
				lineCopy = '\n\t' + tempList[0][1:] + tempList[1]
				tempList = tempList[2].partition("</Simula")
				lineCopy += tempList[0].rstrip() + '\n'
				for i in range(indentNumber):
					lineCopy += '\t'
				lineCopy += tempList[1] + tempList[2]
			previousIndentNumber = indentNumber
		if "/>" in lineCopy:
			previousIndentNumber = indentNumber
		if "<?xml" in lineCopy:
			lineCopy = '\n' + lineCopy.strip().rstrip()
		cleanString += lineCopy
	return cleanString			

# This function creates a single XML file from a file that contains references to other files, removes the comments and cleans it up.
def CleanZipFile(inputString, directory):
	temp = CleanFile(inputString)
	lines = temp.splitlines()
	parsedString = ""
	for line in lines:
		if "<?xml" in line:
			continue
		if "SimulationModelIncludeFile" in line:
			continue
		if "SimulaIncludeFile" in line:
			temp = line.partition('fileName="')[2].partition('"')[0]
			subFile = directory + temp
			print(subFile)
			subFileString = ReadFile(subFile)
			parsedString += CleanZipFile(subFileString, directory)
			continue
		parsedString += line + "\n"
	return parsedString

# This function creates a single XML file from a file that contains references to other files and cleans it up.
def ZipFile(inputString, directory):
	temp = CleanFileKeepComments(inputString)
	lines = temp.splitlines()
	parsedString = ""
	comment = False
	for line in lines:
		if "<?xml" in line:
			continue
		if "SimulationModelIncludeFile" in line:
			continue
		lineCopy = line
		if comment is True:
			if line.partition("-->")[1] != "":
				comment = False
			lineCopy = line.partition("-->")[2].rstrip()
		templist = lineCopy.split("<!--")
		lineCopy = templist[0]
		for i in range(1, len(templist)):
			if templist[i].rpartition("-->")[1] != "":
				lineCopy += templist[i].rpartition("-->")[2].rstrip()
			comment = True
		if "-->" in templist[len(templist)-1]:
			comment = False
		if "SimulaIncludeFile" in lineCopy:
			temp = line.partition('fileName="')[2].partition('"')[0]
			subfile = directory + temp
			print(subfile)
			subfileString = ReadFile(subfile)
			parsedString += ZipFile(subfileString, directory)
			continue
		parsedString += line + "\n"
	return parsedString

# This function collects all tags that belong under a given SimulaBase tag, including those that are included by SimulaDirective. Can be used to, for example, gather the shootTemplate in one place.
def CollectSimulaBase(inputString, rootName):
	temp = ExpandDirectives(inputString)
	lines = temp.splitlines()
	parsedString = ""
	closingTag = "</SimulaBase>"
	collectedParameters = ""
	indentNumber = 0
	indentAtFind = 0
	for line in lines:
		if ("<SimulaBase" in line) and ('name="' + rootName + '"' in line):
			parsedString = line.strip().rstrip().partition("</Simula")[0]
			indentAtFind = indentNumber
			indentNumber += 1
			continue
		if ("<SimulaDirective" in line) and ('path="' + rootName + '"' in line) and (indentNumber is indentAtFind):
			if 'path="/' in line:
				ERROR('Path starting with "/" in file! This is an absolute path but this code is currently not able to deal with this. Replace any absolute paths by appropriate relative paths before continuing')
			indentNumber += 1
			continue
		if indentNumber > 0:
			temp = indentNumber
			indentNumber -= line.count("</Simula")
			indentNumber -= line.count("/>")
			indentNumber += line.count("<Simula")
			if indentNumber == 0:
				continue
			if temp > indentNumber:
				temp = indentNumber
			for i in range(temp):
				collectedParameters += '\t'
			collectedParameters += line.strip().rstrip() + '\n'
	parsedString += '\n' + collectedParameters + closingTag
	return parsedString

# This function removes all SimulaDirectives from the input file by collecting the tags under SimulaDirectives under the corresponding SimulaBase tags.
def CondenseSimulaBases(inputString):
	parsedString = CleanFile(inputString)
	parsedString = ExpandDirectives(parsedString)
	searchingDepth = 1
	while(True):
		print("Searching at depth " + str(searchingDepth))
		lines = parsedString.splitlines()
		parsedString = ""
		indentNumber = 0
		toCondenseString = ""
		foundAtThisDepth = False
		for line in lines:
			line = line + "\n"
			indentNumber -= line.count("</Simula")
			indentNumber -= line.count("/>")
			indentNumber += line.count("<Simula")
			if indentNumber >= searchingDepth:
				toCondenseString += line
				foundAtThisDepth = True
				continue
			if indentNumber < searchingDepth:
				if not toCondenseString == "":
					parsedString += CondenseSubTree(toCondenseString, indentNumber)
					toCondenseString = ""
				parsedString += line
		if foundAtThisDepth == False:
			break
		if searchingDepth > 10:
			break
		searchingDepth += 1
	parsedString = CleanFile(parsedString)
	return parsedString
	
# This function is used by the above CondenseSimulaBases function to remove SimulaDirectives from input files.
def CondenseSubTree(inputString, baseIndent):
	lines = inputString.splitlines()
	parsedString = ""
	indentNumber = 0
	directoryDict = {}
	openingDict = {}
	closingDict = {}
	directoryName = ""
	directoryContents = ""
	beginString = ""
	endString = ""
	beginDone = False
	addContents = False
	addClosing = False
	searchingDepth = 1
	for line in lines:
		line = line + "\n"
		plusCount = line.count("<Simula")
		minCount = line.count("/>") + line.count("</Simula")
		if not indentNumber == searchingDepth and not addContents:
			if beginDone:
				endString += line
			else:
				beginString += line
			indentNumber += plusCount - minCount
			continue
		if not addContents:
			if plusCount - minCount == 0:
				if not "</Simula" in line:
					if "<SimulaBase" in line:
						directoryName = line.partition('name="')[2].partition('"')[0]
						if directoryName in directoryDict.keys():
							ERROR("Duplicate name: " + line)
						openingDict[directoryName] = line.partition("/>")[0] + ">\n"
						directoryDict[directoryName] = ""
						closingDict[directoryName] = line.partition("<SimulaBase")[0] + "</SimulaBase>\n"
						continue
					if beginDone:
						endString += line
					else:
						beginString += line
					indentNumber += plusCount - minCount
					continue
				if "</Simula" in line:
					tempPart = line.partition(">")
					tempPart2 = tempPart[2].rpartition("</Simula")
					if 'name="' in line:
						directoryName = line.partition('name="')[2].partition('"')[0]
						if directoryName in directoryDict.keys():
							ERROR("Duplicate name: " + line)
						openingDict[directoryName] = tempPart[0] + tempPart[1]
						directoryDict[directoryName] = tempPart2[0] + "\n"
						closingDict[directoryName] = tempPart2[1] + tempPart2[2]
						continue
					if 'name_column2="' in line:
						directoryName = line.partition('name_column2="')[2].partition('"')[0]
						if directoryName in directoryDict.keys():
							ERROR("Duplicate name: " + line)
						openingDict[directoryName] = tempPart[0] + tempPart[1]
						directoryDict[directoryName] = tempPart2[0] + "\n"
						closingDict[directoryName] = tempPart2[1] + tempPart2[2]
						continue
					if tempPart2[0].strip == "":
						continue
					if not 'path="' in line:
						ERROR("Inconsistent line: " + line)
					directoryName = line.partition('path="')[2].partition('"')[0]
					directoryDict[directoryName] += tempPart2[0] + "\n"
					continue
			beginDone = True
			addContents = True
			if 'name="' in line:
				directoryName = line.partition('name="')[2].partition('"')[0]
				if directoryName in directoryDict.keys():
					ERROR("Duplicate entry: " + line)
				openingDict[directoryName] = line
				directoryDict[directoryName] = ""
				addClosing = True
			elif 'name_column2="' in line:
				directoryName = line.partition('name_column2="')[2].partition('"')[0]
				if directoryName in directoryDict.keys():
					ERROR("Duplicate entry: " + line)
				openingDict[directoryName] = line
				directoryDict[directoryName] = ""
				addClosing = True
			elif 'path="' in line:
				directoryName = line.partition('path="')[2].partition('"')[0]
				if directoryName[0] == "/":
					print("Warning: deleting '/' at start of path " + directoryName + ". This may have unintended consequences")
					directoryName = directoryName[1:]
				addClosing = False
			else:
				ERROR("Inconsistent line: " + line)
			indentNumber += plusCount - minCount
			continue
		indentNumber += plusCount - minCount
		if indentNumber == searchingDepth and addContents:
			directoryDict[directoryName] += directoryContents
			if addClosing:
				closingDict[directoryName] = line
				addClosing = False
			addContents = False
			directoryName = ""
			directoryContents = ""
			continue
		if addContents:
			directoryContents += line
	parsedString = beginString
	for key in directoryDict.keys():
		parsedString += openingDict[key] + directoryDict[key] + closingDict[key]
	parsedString += endString
	return parsedString

# This function is used by the above CondenseSimulaBases function to remove SimulaDirectives from input files.
def CondenseSubTreeOld(inputString, baseIndent):
	lines = inputString.splitlines()
	parsedString = ""
	indentNumber = 0
	directoryList = [[""]]
	closingTags = {}
	directoryContents = ""
	addContents = False
	currentDirectoryNumber = 0
	moveToDirectoryNumber = 0
	searchingDepth = 1
	addToSelfContainedTag = False
	selfContainedTagIndex = 0
	selfContainedTagCases = [[""]]
	selfContainedTagContents = ""
	for line in lines:
		line = line + "\n"
		temp = line
		indentNumber -= line.count("</Simula")
		indentNumber -= line.count("/>")
		indentNumber += line.count("<Simula")
		if addToSelfContainedTag is True:
			temp = ""
			if indentNumber == searchingDepth:
				addToSelfContainedTag = False
				if len(selfContainedTagCases[selfContainedTagIndex]) == 1:
					ERROR("Error 1: " +  str(moveToDirectoryNumber))
				selfContainedTagCases[selfContainedTagIndex].append(selfContainedTagContents)
				selfContainedTagContents = ""
		if addToSelfContainedTag is True:
			selfContainedTagContents += line
		if addContents is True:
			temp = ""
			if indentNumber == searchingDepth:
				addContents = False
				if len(directoryList[moveToDirectoryNumber]) == 1:
					ERROR("Error 1: " +  str(moveToDirectoryNumber))
				directoryList[moveToDirectoryNumber].append(directoryContents)
				if not '</SimulaDirective' in line:
					closingTags[moveToDirectoryNumber] = line
				directoryContents = ""
		if '<SimulaDirective' in line and '</SimulaDirective' in line:
			continue
		if addContents is True:
			directoryContents += line

		if indentNumber == searchingDepth and '<Simula' in line and '</Simula' in line and 'name=' in line:
			moveToDirectoryNumber = currentDirectoryNumber + 1
			currentDirectoryNumber = moveToDirectoryNumber + 1
			closingTags[moveToDirectoryNumber] = ""
			for i in range(indentNumber + baseIndent):
				closingTags[moveToDirectoryNumber] += '\t'
			closingTags[moveToDirectoryNumber] += line.partition('</Simula')[1] + line.partition('</Simula')[2]	
			line = line.partition('</Simula')[0] + '\n'
			directoryContents = ""
			directoryName = line.partition('name="')[2].partition('"')[0]
			directoryList.append([directoryName, indentNumber, line])
			directoryList.append([""])
			temp = ""
		if indentNumber - 1 == searchingDepth and addContents is False and (('Simula' in line and 'name=' in line and not ('</Simula' in line or '/>' in line)) or ('<SimulaBase' in line and 'name=' in line)):
			moveToDirectoryNumber = currentDirectoryNumber + 1
			currentDirectoryNumber = moveToDirectoryNumber + 1
			directoryContents = ""
			directoryName = line.partition('name="')[2].partition('"')[0]
			if directoryName == "":
				ERROR("Error 2: " + line)
			addContents = True
			directoryList.append([directoryName, indentNumber - 1, line])
			directoryList.append([""])
			temp = ""
		if indentNumber - 1 == searchingDepth and '<SimulaDirective' in line:
# NEED TO UPDATE TO ALLOW FOR paths that start with / (these are absolute paths, not relative)
			if 'path="/' in line:
				ERROR('Path starting with "/" in file! This is an absolute path but this code is currently not able to deal with this. Replace any absolute paths by appropriate relative paths before continuing')
			directoryName = line.partition('path="')[2].partition('"')[0]
			temp = ""
			directoryContents = ""
			tempFound = False
			for i in range(len(directoryList)):
				if directoryList[i][0] == directoryName:
					moveToDirectoryNumber = i
					addContents = True
					tempFound = True
			if tempFound == False:
				addToSelfContainedTag = True
				selfContainedTagIndex += 1
				selfContainedTagCases.append([selfContainedTagIndex, directoryName])
			if directoryName == "":
				ERROR("Error 3: " + str(moveToDirectoryNumber) + " " + str(line))
		directoryList[currentDirectoryNumber][0] += temp
	for i in range(len(directoryList)):
		if len(directoryList[i]) == 1:
			parsedString += directoryList[i][0]
			continue
		for j in range(2, len(directoryList[i])):
			parsedString += directoryList[i][j]
		try:
			parsedString += closingTags[i]
		except:
			ERROR("Error 4: " + str(i) + " " + str(directoryList[i][1]))
	parsedString = InsertIntoSelfContainedTag(parsedString, selfContainedTagCases)
	return parsedString

# This function is used by the above function to handle an edge case
def InsertIntoSelfContainedTag(inputString, selfContainedTagList):
	parsedString = inputString
	for entry in selfContainedTagList:
		if len(entry) == 1:
			continue
		tag = ""
		lines = parsedString.splitlines()
		parsedString = ""
		for line in lines:
			if "\t<Simula" in line and entry[1] in line:
				tag = line.partition('<')[2].split()[0]
				parsedString += line.replace("/>", ">") + "\n"
				parsedString += entry[2]
				parsedString += line.partition("<")[0] + "</" + tag + ">\n"
				continue
			parsedString += line + "\n"
	return parsedString

# This function sorts a file alphabetically at every level
def AlphabeticallySortXML(inputString):
	parsedString = CleanFile(inputString)
	searchingDepth = 1
	while(True):
		print("Sorting at depth " + str(searchingDepth))
		lines = parsedString.splitlines()
		parsedString = ""
		indentNumber = 0
		toSortString = ""
		foundAtThisDepth = False
		for line in lines:
			line = line + "\n"
			indentNumber -= line.count("</Simula")
			indentNumber -= line.count("/>")
			indentNumber += line.count("<Simula")
			if indentNumber >= searchingDepth:
				toSortString += line
				foundAtThisDepth = True
				continue
			if indentNumber < searchingDepth:
				if not toSortString == "":
					parsedString += SortSubTree(toSortString, indentNumber)
					toSortString = ""
				parsedString += line
		if foundAtThisDepth == False:
			break
		if searchingDepth > 10:
			break
		searchingDepth += 1
	parsedString = CleanFile(parsedString)
	return parsedString

# This function is used by the above AlphabeticallySortXML function to sort an input file
def SortSubTree(inputString, indentNumber):
	lines = inputString.splitlines()
	parsedString = ""
	indentNumber = 0
	searchingDepth = 1
	tagDict = {}
	nameList = []
	recordingTag = False
	tempName = ""
	tempTag = ""
	beginString = ""
	endString = ""
	beginDone = False
	for line in lines:
		line = line + "\n"
		temp = line
		plusCount = line.count("<Simula")
		minCount = line.count("/>") + line.count("</Simula")
		if not indentNumber == searchingDepth and not recordingTag:
			if beginDone:
				endString += line
			else:
				beginString += line
			indentNumber += plusCount - minCount
			continue
		if not recordingTag:
			beginDone = True
			recordingTag = True
			if 'name="' in line:
				tempName = line.partition('name="')[2].partition('"')[0]
			elif 'name_column2="' in line:
				tempName = line.partition('name_column2="')[2].partition('"')[0]
			elif 'path="' in line:
				tempName = line.partition('path="')[2].partition('"')[0]
			else:
				ERROR("Inconsistent line: " + line)
		indentNumber += plusCount - minCount
		if recordingTag:
			tempTag += line
			if indentNumber == searchingDepth:
				recordingTag = False
				nameList.append(tempName)
				tagDict[tempName] = tempTag
				tempName = ""
				tempTag = ""
				continue
	nameList = sorted(nameList)
	parsedString = beginString
	for name in nameList:
		parsedString += tagDict[name]
	parsedString += endString
	return parsedString
	
# This function sorts the properties in tags
def SortTags(inputString):
	lines = inputString.splitlines()
	parsedString = ""
	for line in lines:
		if not "<Simula" in line:
			parsedString += line + "\n"
			continue
		temp = line.partition("<Simula")
		temp2 = temp[2].replace("\t", " ").partition(" ")
		temp3 = temp2[2].partition(">")
		buildString = temp[0] + temp[1] + temp2[0]
		sortString = temp3[0]
		endString = ">" + temp3[2]
		if temp3[0][-1] == "/":
			endString = "/" + endString
			sortString = sortString[:-1]
		toSort = SplitTagProperties(sortString)
		toSort = sorted(toSort)
		firstEntry = "NotFound"
		for i in range(len(toSort)):
			if "name=" in toSort[i] or "path=" in toSort[i] or "name_column2=" in toSort[i]:
				firstEntry = i
		if firstEntry == "NotFound":
			ERROR("Inconsistent tag: " + line)
		buildString += " " + toSort[firstEntry]
		toSort.pop(firstEntry)
		for entry in toSort:
			buildString += " " + entry
		buildString += endString
		parsedString += buildString + "\n"
	return parsedString

def SplitTagProperties(inputString):
	inputString = inputString.replace("\t", " ")
	splitList = []
	property = ""
	finalQM = False
	i = 0
	while True:
		if i >= len(inputString):
			break
		if inputString[i] == '"':
			if finalQM:
				property += inputString[i]
				splitList.append(property)
				property = ""
				finalQM = False
				i += 2
			else:
				finalQM = True
				property += inputString[i]
				i += 1
			continue
		property += inputString[i]
		i += 1
	return splitList

# This function expands SimulaDirective paths that are of the form path1/path2/.../pathn out into separate tags.
def ExpandDirectives(inputString):
	temp = CleanFileKeepComments(inputString)
	lines = temp.splitlines()
	parsedString = ""
	indentNumber = 0
	indentAtSplit = 0
	splitNumber = 0
	repeat = 0
	comment = False
	for line in lines:
		line = line
		if line[0:1] == "$":
			parsedString += line
			continue
		if "<!--" in line:
			if not "-->" in line.rpartition("<!--")[2]:
				comment = True
			else:
				parsedString += line + "\n"
				continue
		if "-->" in line:
			comment = False
			parsedString += line + "\n"
			continue
		if comment:
			parsedString += line + "\n"
			continue
		if ("<SimulaDirective" in line) and ("</SimulaDirective>" in line) and not ("<Simula" in line.partition("<SimulaDirective")[2].rpartition("</SimulaDirective>")[0]):
			continue
		if ("<SimulaDirective" in line) and ("/" in line):
			if indentAtSplit > 0:
				repeat = 1
				parsedString += line + "\n"
				indentNumber -= line.count("</Simula")
				indentNumber -= line.count("/>")
				indentNumber += line.count("<Simula")
				continue
			lineCopy = ""
			if 'path="' in line:
				lineCopy = line.partition('path="')[2].partition('"')[0]
			if lineCopy == "":
				ERROR("The following line in the file is not well-formed: " + line)
			temp = lineCopy.split('/')
			# If the path starts with /, it's an absolute path and we should add it to the first component of the path
			if temp[0] == "":
				temp[1] = "/" + temp[1]
			tempList = []
			for entry in temp:
				if entry == "":
					continue
				tempList.append(entry)
			splitNumber = len(tempList)
			if splitNumber == 1:
				parsedString += line.partition('path=')[0] + 'path="' + tempList[0] + '"' + line.partition(lineCopy)[2][1:] + "\n"
				indentNumber += 1
				splitNumber = 0
				continue
			temp = ""
			for i in range(splitNumber):
				for j in range(i+indentNumber):
					temp += '\t'
				temp += '<SimulaDirective path="' + tempList[i] + '">\n'
			indentAtSplit = indentNumber
			indentNumber += 1
			parsedString += temp
			continue
		indentNumber -= line.count("</Simula")
		indentNumber -= line.count("/>")
		indentNumber += line.count("<Simula")
		if indentNumber < 0:
			parsedString += line
			break
		if splitNumber > 0:
			if indentNumber == indentAtSplit:
				for i in range(splitNumber):
					for j in range(indentNumber + splitNumber - i-1):
						parsedString += '\t'
					parsedString += '</SimulaDirective>\n'
				splitNumber = 0
				indentAtSplit = 0
				continue
			for i in range(splitNumber-1):
				parsedString += '\t'
		parsedString += line + "\n"
	if repeat == 1:
		parsedString = ExpandDirectives(parsedString)
	return parsedString

# This function replaces strings in input files.
def ReplaceParameter(inputString, toReplace, replaceString):
	lines = inputString.splitlines()
	parsedString = ""
	replaceString = str(replaceString)
	for line in lines:
		parsedString += line.replace(toReplace, replaceString)
	return parsedString

# This function replaces the contents of a path in an input file and saves it to an output file.
def PathReplaceSaveToFile(inputFile, path, replaceTraits, replaceValues, exportFile):
	fileString = ReadFile(inputFile)
	cleanedString = CleanFile(fileString)
	fileString = ExpandDirectives(cleanedString)
	changedString = ReplaceInPath(fileString, path, replaceTraits, replaceValues)
	with open(exportFile, 'wb') as f:
		f.write(changedString.encode('utf-8'))
		f.close()	

# This function replaces the contents of a path in an input file.
def ReplaceInPath(inputString, path, replaceTraits, replaceValues):
	if not len(replaceTraits) == len(replaceValues):
		ERROR("Number of attributes to change and number of new values do not match!")
	etree = ""
	with tempfile.TemporaryFile() as f:
		f.write(inputString.strip().encode('utf-8'))
		f.seek(0)
		etree = ET.parse(f)
		f.close()
	splitPath = path.split('\\')
	location = FindXMLElement(etree, splitPath)
	for i in range(len(replaceTraits)):
		ChangeXMLElementAttribute(location, replaceTraits[i], replaceValues[i])
	returnString = ""
	with tempfile.TemporaryFile() as f:
		etree.write(f)
		f.seek(0)
		for line in f:
			returnString += line.decode('utf-8', 'ignore')
		f.close()
	return returnString

# This function finds the element given by a path in an XML etree.
def FindXMLElement(etree, elementPath):
	probeList = [etree.getroot()]
	for i in range(len(elementPath) - 1):
		tempList = probeList
		probeList = []
		for probe in tempList:
			probeList += GetXMLChildElement(probe, elementPath[i])
	foundList = []
	for probe in probeList:
		if (GetXMLBottomElement(probe, elementPath[len(elementPath)-1])):
			foundList += GetXMLBottomElement(probe, elementPath[len(elementPath)-1])
	if len(foundList) > 1:
		ERROR("More than 1 element found for the path " + elementPath)
		return False
	if len(foundList) == 1:
		return foundList[0]
	if len(foundList) == 0:
		print("ERROR: XML element ", elementPath, " not found!")
		sys.exit()
		return False

# This function gets a child element of an element in an XML etree.
def GetXMLChildElement(parent, child):
	possibilities = parent.findall('./SimulaBase[@name="' + child + '"]')
	possibilities += parent.findall('./SimulaDirective[@path="' + child + '"]')
	possibilities += parent.findall('./SimulaTable[@name_column2="' + child + '"]')
	possibilities += parent.findall('./SimulaTable[@name_colum2="' + child + '"]')
	possibilities += parent.findall('./SimulaConstant[@name="' + child + '"]')
	return possibilities

# This function gets a child element of an element in an XML etree. Must be the bottom element.
def GetXMLBottomElement(parent, child):
	possibilities = []
	possibilities += parent.findall("./*[@name='" + child + "']")
	possibilities += parent.findall("./*[@name_column2='" + child + "']")
	if len(possibilities) > 1:
		ERROR("More than 1 bottom element found")
		return False
	return possibilities

# This function changes an attribute in a tag in an XML etree
def ChangeXMLElementAttribute(element, attribute, value):
	if attribute == "text":
		element.text = value
	elif len(attribute.split(';')) == 2:
		attributeSplit = attribute.split(';')
		if attributeSplit[0] == "STEC":
			temp = element.text.strip().split()
			temp[int(attributeSplit[1])] = value
			element.text = ' '.join(temp)
	else:
		element.set(attribute, value)
	return element

# This function takes a recipe for a factorial experimental design and creates an instruction file that can be used to generate the XML input files by the function below.
#A sample recipe is included in the "ExampleInputs" directory. In brief, a recipe should be structured as follows:
#It should be a csv file. Every line needs to contain at least 4 entries: First, the name of the phene or input parameter you are varying. Then the path of the tag that needs to be altered. Then at least 2 values to be put into this tag. You can add any number of values after these two, just separate them by commas.
#Often, varying a single phene will entail editing multiple tags. Because of this, tags preceded by the same name will be varied as a group. So the number of unique names starting each line will be the number of levels of your factorial design. The example file has 6 levels.
#If you want to alter not the value inside a tag, but an attribute of the tag (for example the "mean" of a "SimulaStochastic" tag), simply type attributeName;value (for example: mean;0.5) instead of just a value. You can alter multiple of these in one line by stringing them together with "+". For example: mean;0.5+minimum;0.1+maximum;1.0. See the example file.
def CreateFactorialInstructionFile(inputFile, outputFile = "", namesFile = ""):
# Every entry in level will be a list, the first entry of which is the name of the variable being varied, the second the number of different values it has, the third a list of XML paths that will be changed, with the corresponding values for each of these levels.
	levels = []
	with open(inputFile, 'rb') as f:
		for line in f:
			line = line.decode('utf-8', 'ignore')
			splitLine = [entry.strip() for entry in line.split(',')]
			temp = []
			for i in range(len(splitLine) - 1):
				temp.append(splitLine[i + 1])
			if not splitLine[0] in [level[0] for level in levels]:
				levels.append([splitLine[0], len(splitLine) - 2, [temp]])
			else:
				levels[[level[0] for level in levels].index(splitLine[0])][2].append(temp)
		f.close()
	totalLines = 1
	totalPaths = 0
	for level in levels:
		totalLines *= level[1]
		totalPaths += len(level[2])
	header = []
	for level in levels:
		for path in level[2]:
			header.append(path[0])
	data = [["" for j in range(totalPaths + 1)] for i in range(totalLines)]
	period = totalLines
	previousPaths = 0
	for i, level in enumerate(levels):
		for j, entry in enumerate(data):
			entry[0] = entry[0] + str(int((j%period)/(period/level[1]))) + "_"
			for k, path in enumerate(level[2]):
				entry[1 + previousPaths + k] = path[1 + int((j%period)/(period/level[1]))]
		period /= level[1]
		previousPaths += len(level[2])
	for entry in data:
		entry[0] = entry[0][:-1]
	if outputFile == "":
		outputFile = inputFile.rpartition(".")[2] + "Factorial.csv"
	with open(outputFile, 'wb') as f:
		f.write("Identifier".encode('utf-8'))
		for entry in header:
			f.write((", " + entry).encode('utf-8'))
		f.write('\n'.encode('utf-8'))
		tempString = ""
		for row in data:
			for entry in row:
				tempString += entry + ", "
			tempString = tempString[:-2] + "\n"
		tempString = tempString[:-1]
		f.write(tempString.encode('utf-8'))
		f.close()
	if namesFile == "":
		namesFile = namesFile.rpartition(".")[2] + "Identifiers.txt"
	with open(namesFile, 'wb') as f:
		f.write(data[0][0].encode('utf-8'))
		for i in range(len(data) - 1):
			f.write(('\n' + data[i+1][0]).encode('utf-8'))
		f.close()

# This function takes an instruction file and an XML input file as inputs to create XML input files with the appropriate replacements.
# The instruction file that this script needs can be created from a recipe with the script above or by hand. See the "ExampleInputs" directory for an example of an instruction file created from the recipe in that directory. Try applying it to the example XML file in the same repository! It will create 216 XML files, your factorial experiment!
def WriteXMLFilesFromInstructionFile(xmlFile, instructionFile, namePrefix = ""):
	paths = []
	data = []
	with open(instructionFile, 'rb') as f:
		for line in f:
			line = line.decode('utf-8', 'ignore')
			if paths == []:
				paths = [elt.strip() for elt in line.split(',')[1:]]
			else:
				data.append([elt.strip() for elt in line.split(',')])
		f.close()
	xmlString = ReadFile(xmlFile)
	xmlString = CleanFile(xmlString)
	xmlString = ExpandDirectives(xmlString)
	numberOfPaths = len(data[0]) - 1
	for n, row in enumerate(data):
		outputString = xmlString
		for i in range(numberOfPaths):
			valueSplit = row[1 + i].split("+")
			replaceTraits = []
			replaceValues = []
			for trait in valueSplit:
				splitTrait = trait.split(';')
				if len(splitTrait) == 1:
					replaceTraits.append("text")
					replaceValues.append(splitTrait[0])
				elif len(splitTrait) == 2:
					replaceTraits.append(splitTrait[0])
					replaceValues.append(splitTrait[1])
				elif len(splitTrait) == 3:
					replaceTraits.append(splitTrait[0] + ';' + splitTrait[1])
					replaceValues.append(splitTrait[2])
					
			outputString = ReplaceInPath(outputString, paths[i], replaceTraits, replaceValues)
		with open(namePrefix + row[0] + ".xml", 'wb') as f:
			f.write(outputString.encode('utf-8'))
			f.close()
		ProgressBar(float(n)/float(len(data)))
		
# This function takes an instruction file and an XML input file as inputs to create XML input files with the appropriate RNG seeds.
def AddRNGSeedsToXMLFile(xmlFile, RNGSeedFile, namePrefix = ""):
	seeds = []
	with open(RNGSeedFile, 'rb') as f:
		for line in f:
			line = line.decode('utf-8', 'ignore')
			seeds.append(line.strip())
		f.close()
	xmlString = ReadFile(xmlFile)
	xmlString = CleanFile(xmlString)
	xmlString = ExpandDirectives(xmlString)
	for n, row in enumerate(seeds):
		outputString = ReplaceInPath(xmlString, "simulationControls\\randomNumberGeneratorSeed", ["text"], [row])
		with open(namePrefix + seeds[n] + ".xml", 'wb') as f:
			f.write(outputString.encode('utf-8'))
			f.close()
		ProgressBar(float(n)/float(len(seeds)))





