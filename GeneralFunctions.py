# OpenSimRoot Tools - Copyright (C) - 2019 - Ernst Schäfer
# All rights reserved.
#
# This file is part of OpenSimRoot Tools.
#
# OpenSimRoot Tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# OpenSimRoot Tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This file contains a collection of generic functions and definitions.

import sys
import glob
import os


def ERROR(errstr):
	print('FATAL ERROR:',errstr)
	sys.exit()
	
def ReadFile(inputFile):
	parsedString = ""
	with open(inputFile, 'rb') as f:
		for line in f:
			line = line.decode('utf-8', 'ignore')
			parsedString += line
		return parsedString

def ProgressBar(progress):
    barLength = 25
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rProgress: [{0}]".format( "#"*block + "-"*(barLength-block))
    sys.stdout.write(text)
    sys.stdout.flush()
	
