# Ernst D Schafer 2020 University of Nottingham
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname( __file__ ), os.pardir)))
from FunctionsXML import *

xmlFile = ""
if sys.argv[1:]:
	xmlFile = sys.argv[1]
else:
	sys.exit("No XML file given")
if xmlFile == "-help" or xmlFile == "-h" or xmlFile == "h" or xmlFile == "help":
	print("This script writes values to XML files according to a recipe file. It takes the following inputs:\n->1: A base XML file\n->2: A file with the replacement recipe\n->3 (optional): A naming prefix for the generated XML files\n->4 (optional): An alternative name for the intermediary instruction file\n->5 (optional): An alternative file for saving the identifier codes")
	sys.exit()
recipeFile = ""
if sys.argv[2:]:
	recipeFile = sys.argv[2]
else:
	sys.exit("No recipe file given")
namePrefix = ""
if sys.argv[3:]:
	namePrefix = sys.argv[3]
instructionFile = recipeFile
if "Recipe" in instructionFile:
	instructionFile = instructionFile.replace("Recipe", "Instructions")
elif "recipe" in instructionFile:
	instructionFile = instructionFile.replace("recipe", "instructions")
else:
	instructionFile = instructionFile.rpartition(".")[0] + "Instructions" + instructionFile.rpartition(".")[2]
if sys.argv[4:]:
	instructionFile = sys.argv[4]
namingFile = "Identifiers.txt"
if sys.argv[5:]:
	namingFile = sys.argv[5]

CreateFactorialInstructionFile(recipeFile, instructionFile, namingFile)
print("Creating files using " + xmlFile + " as base file, with " + instructionFile + " as instruction file. Output names will be " + namePrefix + " followed by a identifying code + .xml\n")
WriteXMLFilesFromInstructionFile(xmlFile, instructionFile, namePrefix)
print("\nDone creating XML files.")
